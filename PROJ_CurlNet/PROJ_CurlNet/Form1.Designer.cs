﻿namespace PROJ_Curl
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.requsetBtn = new System.Windows.Forms.Button();
            this.payloadTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.reqOptPost = new System.Windows.Forms.RadioButton();
            this.reqOptGet = new System.Windows.Forms.RadioButton();
            this.urlTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.payloadLen = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.errCodeTxt = new System.Windows.Forms.TextBox();
            this.caSkipCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Request Type :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "URL :";
            // 
            // requsetBtn
            // 
            this.requsetBtn.Location = new System.Drawing.Point(531, 8);
            this.requsetBtn.Name = "requsetBtn";
            this.requsetBtn.Size = new System.Drawing.Size(85, 39);
            this.requsetBtn.TabIndex = 2;
            this.requsetBtn.Text = "Send Requuest";
            this.requsetBtn.UseVisualStyleBackColor = true;
            this.requsetBtn.Click += new System.EventHandler(this.requsetBtn_Click);
            // 
            // payloadTxt
            // 
            this.payloadTxt.Location = new System.Drawing.Point(93, 131);
            this.payloadTxt.Multiline = true;
            this.payloadTxt.Name = "payloadTxt";
            this.payloadTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.payloadTxt.Size = new System.Drawing.Size(523, 243);
            this.payloadTxt.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(39, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "Payload :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.reqOptPost);
            this.groupBox1.Controls.Add(this.reqOptGet);
            this.groupBox1.Location = new System.Drawing.Point(93, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(115, 56);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // reqOptPost
            // 
            this.reqOptPost.AutoSize = true;
            this.reqOptPost.Location = new System.Drawing.Point(57, 20);
            this.reqOptPost.Name = "reqOptPost";
            this.reqOptPost.Size = new System.Drawing.Size(50, 16);
            this.reqOptPost.TabIndex = 7;
            this.reqOptPost.TabStop = true;
            this.reqOptPost.Text = "POST";
            this.reqOptPost.UseVisualStyleBackColor = true;
            this.reqOptPost.CheckedChanged += new System.EventHandler(this.reqOptPost_CheckedChanged);
            // 
            // reqOptGet
            // 
            this.reqOptGet.AutoSize = true;
            this.reqOptGet.Location = new System.Drawing.Point(6, 20);
            this.reqOptGet.Name = "reqOptGet";
            this.reqOptGet.Size = new System.Drawing.Size(45, 16);
            this.reqOptGet.TabIndex = 6;
            this.reqOptGet.TabStop = true;
            this.reqOptGet.Text = "GET";
            this.reqOptGet.UseVisualStyleBackColor = true;
            this.reqOptGet.CheckedChanged += new System.EventHandler(this.reqOptGet_CheckedChanged);
            // 
            // urlTxt
            // 
            this.urlTxt.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.urlTxt.Location = new System.Drawing.Point(93, 53);
            this.urlTxt.Multiline = true;
            this.urlTxt.Name = "urlTxt";
            this.urlTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.urlTxt.Size = new System.Drawing.Size(523, 42);
            this.urlTxt.TabIndex = 6;
            this.urlTxt.Text = "https://Sandbox-TerminalRouterCastles-192137735.us-east-1.elb.amazonaws.com";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(43, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 24);
            this.label4.TabIndex = 7;
            this.label4.Text = "Payload\r\nLength :";
            // 
            // payloadLen
            // 
            this.payloadLen.AutoSize = true;
            this.payloadLen.Location = new System.Drawing.Point(93, 105);
            this.payloadLen.Name = "payloadLen";
            this.payloadLen.Size = new System.Drawing.Size(36, 12);
            this.payloadLen.TabIndex = 8;
            this.payloadLen.Text = "0 Byte";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(566, 391);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 40);
            this.button1.TabIndex = 9;
            this.button1.Text = "Payload\r\nClear";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 391);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "ErrCode :";
            // 
            // errCodeTxt
            // 
            this.errCodeTxt.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.errCodeTxt.Location = new System.Drawing.Point(93, 391);
            this.errCodeTxt.Multiline = true;
            this.errCodeTxt.Name = "errCodeTxt";
            this.errCodeTxt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.errCodeTxt.Size = new System.Drawing.Size(412, 24);
            this.errCodeTxt.TabIndex = 11;
            // 
            // caSkipCheckBox
            // 
            this.caSkipCheckBox.AutoSize = true;
            this.caSkipCheckBox.Location = new System.Drawing.Point(236, 19);
            this.caSkipCheckBox.Name = "caSkipCheckBox";
            this.caSkipCheckBox.Size = new System.Drawing.Size(96, 16);
            this.caSkipCheckBox.TabIndex = 13;
            this.caSkipCheckBox.Text = "Skip Certificate";
            this.caSkipCheckBox.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(628, 532);
            this.Controls.Add(this.caSkipCheckBox);
            this.Controls.Add(this.errCodeTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.payloadLen);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.urlTxt);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.payloadTxt);
            this.Controls.Add(this.requsetBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "CurlNet";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button requsetBtn;
        private System.Windows.Forms.TextBox payloadTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton reqOptPost;
        private System.Windows.Forms.RadioButton reqOptGet;
        private System.Windows.Forms.TextBox urlTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label payloadLen;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox errCodeTxt;
        private System.Windows.Forms.CheckBox caSkipCheckBox;
    }
}

