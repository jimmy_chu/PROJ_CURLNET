﻿using System;
using System.Windows.Forms;
using SeasideResearch.LibCurlNet;

namespace PROJ_Curl
{
    public partial class Form1 : Form
    {
        int iReqType;//0:GET, 1:POST
        String sUserAgent = "";
        Easy easy;
        String reqURL = "";
        String caCert = "ca-bundle.crt";
        CURLcode ret;
        int iLen;

        public Form1()
        {
            InitializeComponent();
            
            reqOptGet.Checked = true;

            if (reqOptPost.Checked == true)
                iReqType = 1;
            else
                iReqType = 0;
        }

        private void requsetBtn_Click(object sender, EventArgs e)
        {
            clearMsg();
            if (urlTxt.Text == "") {
                MessageBox.Show("URL is empty !", "Warning");
                return;
            }

            try
            {
                Curl.GlobalInit((int)CURLinitFlag.CURL_GLOBAL_ALL);

                easy = new Easy();

                Easy.WriteFunction wf = new Easy.WriteFunction(OnWriteData);
                easy.SetOpt(CURLoption.CURLOPT_WRITEFUNCTION, wf);

                if (urlTxt.ToString().Contains("https"))
                { 

                    if (caSkipCheckBox.Checked == true)
                    {
                        easy.SetOpt(CURLoption.CURLOPT_SSL_VERIFYHOST, 0L);
                        easy.SetOpt(CURLoption.CURLOPT_SSL_VERIFYPEER, 0L);
                    }
                    else
                    {
                        Easy.SSLContextFunction sf = new Easy.SSLContextFunction(OnSSLContext);
                        //easy.SetOpt(CURLoption.CURLOPT_SSL_VERIFYHOST, 1L);
                        //easy.SetOpt(CURLoption.CURLOPT_SSL_VERIFYPEER, 1L);
                        easy.SetOpt(CURLoption.CURLOPT_SSL_CTX_FUNCTION, sf);
                        easy.SetOpt(CURLoption.CURLOPT_CAINFO, caCert);
                    }
                }
                //if (urlTxt.ToString().Contains("https") && caSkipCheckBox.Checked == false)
                //{
                    //easy.SetOpt(CURLoption.CURLOPT_CAINFO, caCert);
                    //easy.SetOpt(CURLoption.CURLOPT_CAPATH, caCert);
                //}

                if(reqOptGet.Checked == true)
                    easy.SetOpt(CURLoption.CURLOPT_HTTPGET, true);
                if(reqOptPost.Checked == true)
                    easy.SetOpt(CURLoption.CURLOPT_HTTPPOST, true);

                if (iReqType == 0)
                    easy.SetOpt(CURLoption.CURLOPT_HTTPGET, 1);

                sUserAgent = "Mozilla 4.0 (compatible; MSIE 6.0; Win32";
                easy.SetOpt(CURLoption.CURLOPT_USERAGENT, sUserAgent);
                easy.SetOpt(CURLoption.CURLOPT_ENCODING, "identity");
                easy.SetOpt(CURLoption.CURLOPT_HEADER, 1);

                reqURL = urlTxt.Text.ToString();

                easy.SetOpt(CURLoption.CURLOPT_URL, reqURL);
               
                ret = easy.Perform();
                if (ret != CURLcode.CURLE_OK)
                    errCodeTxt.Text = ret.ToString();

                payloadLen.Text = iLen.ToString() + " Bytes";

                easy.Cleanup();

                Curl.GlobalCleanup();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Exception");
            }
        }

        public Int32 OnWriteData(Byte[] buf, Int32 size, Int32 nmemb, Object extraData)
        {
            payloadTxt.Text = System.Text.Encoding.UTF8.GetString(buf);
            iLen = payloadTxt.TextLength;

            return size * nmemb;
        }

        public static CURLcode OnSSLContext(SSLContext ctx, Object extraData)
        {
            // To do anything useful with the SSLContext object, you'll need
            // to call the OpenSSL native methods on your own. So for this
            // demo, we just return what cURL is expecting.
            return CURLcode.CURLE_OK;
        }

        private void reqOptGet_CheckedChanged(object sender, EventArgs e)
        {
            if (reqOptGet.Checked == true)
                iReqType = 0;
        }

        private void reqOptPost_CheckedChanged(object sender, EventArgs e)
        {
            if (reqOptPost.Checked == true)
                iReqType = 1;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            //if (payloadTxt.Text.ToString().Length != 0)
            {
                clearMsg();
            }
        }
        private void clearMsg() { 
            payloadTxt.Text = "";
            payloadLen.Text = "0 Bytes";
            errCodeTxt.Text = "";
        }
    }
}
